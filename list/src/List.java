import java.util.ArrayList;
import java.util.Collections;
import java.util.*;
import java.lang.*;

public class List {
    public static void main(String[] args) {
        System.out.println("\n");
        System.out.println("__List__");

        ArrayList list = new ArrayList();

        list.add("Horse");
        list.add("Shark");
        list.add("Mouse");
        list.add("Octopus");
        list.add("Giraffe");
        list.add("Rhino");
        list.add("Llama");

        Collections.sort(list);

        System.out.println(list);


        System.out.println("\n");
        System.out.println("__Tree Set__");
        Set set = new TreeSet();
        set.add("Horse");
        set.add("Shark");
        set.add("Mouse");
        set.add("Octopus");
        set.add("Rhino");
        set.add("Llama");
        set.add("Giraffe");

        for (Object str : set) {
            System.out.println((String) str);
        }


        System.out.println("\n");
        System.out.println("__Queue__");
        Queue queue = new PriorityQueue();
        queue.add("Horse");
        queue.add("Shark");
        queue.add("Mouse");
        queue.add("Giraffe");
        queue.add("Octopus");
        queue.add("Rhino");
        queue.add("Llama");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("\n");
        System.out.println("__Set__");
        HashSet hashSet = new HashSet();
        hashSet.add("Horse");
        hashSet.add("Giraffe");
        hashSet.add("Shark");
        hashSet.add("Mouse");
        hashSet.add("Octopus");
        hashSet.add("Rhino");
        hashSet.add("Llama");

        for (Object str : hashSet) {
            System.out.println((String) str);
        }


        System.out.println("\n");
        ArrayList<Books> blist = new ArrayList<Books>();
        blist.add(new Books("Les Miserable", "Hugo, Victor"));
        blist.add(new Books("Fablehaven - Rise of the Evening Star", "Mull, Brandon"));
        blist.add(new Books("Pride and Prejudice", "Austen, Jane"));
        blist.add(new Books("The Alchemist: The Secrets of the Immortal Nicholas Flamel", "Scott, Michael"));
        blist.add(new Books("Fablehaven", "Mull, Brandon"));
        blist.add(new Books("The Lightning Thief", "Riordan, Rick"));
        blist.add(new Books("Belgarath the Sorcerer", "Eddings, David and Leigh"));

        System.out.println("__Books__");
        for (Books books : blist) {
            System.out.format("%s, by %s\n", books.getTitle(), books.getAuthor());
        }
        System.out.println("");

        System.out.println("__Books by Title__");
        Collections.sort(blist, Books.titleComparator);
        for (Books books : blist) {
            System.out.format("%s, by %s\n", books.getTitle(),books.getAuthor());
        }
        System.out.println("");

        System.out.println("__Books by Author__");
        Collections.sort(blist, Books.authorComparator);
        for (Books books : blist) {
            System.out.format("%s, by %s\n", books.getTitle(), books.getAuthor());
        }

    }


}




