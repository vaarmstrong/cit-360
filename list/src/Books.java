import java.util.*;

public class Books {

    private final String title;
    private final String author;

    public Books(String title, String author) {
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    public int compareTo(Books other) {
        int comparison = this.title.compareTo(other.title);
        if (comparison !=0) {
            return comparison;
        }

        return this.author.compareTo(other.author);
    }

    public static final Comparator<Books> titleComparator = new Comparator<Books>() {
        public int compare(Books a, Books b) {
            return a.title.compareTo(b.title);
        }
    };

    public static final Comparator<Books> authorComparator = new Comparator<Books>() {
        public int compare(Books a, Books b) {
            return a.author.compareTo(b.author);
        }
    };

    public String toString() {
        return "Title: " + title + "  Author: " + author;
    }

    public void setTitle(String then_she_was_gone) {
    }

    public void setAuthor(String lisa_jewell) {
    }

}