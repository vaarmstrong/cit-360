import java.util.Scanner;
import static java.lang.System.out;

public class ExceptionB {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        out.println("Input number 1 : ");
        double a = scanner.nextDouble();
        out.println("Input number 2 : ");
        double b = scanner.nextDouble();
        if (b != 0) {
        }
        else {
            out.println("Can't divide by 0. Input another number.");
            out.println("Input number 2 : ");
            b = scanner.nextDouble();
        }
        double c = a / b;
        System.out.println("The answer is " + c);
    }
}



