import java.util.Scanner;

import static java.lang.System.out;

public class Exception {
    public static void main(String[] args) {
        try{
            Scanner scanner = new Scanner(System.in);
            out.println("Input number 1 : ");
            int a = scanner.nextInt();
            out.println("Input number 2 : ");
            int b = scanner.nextInt();
            int c = a / b;
            out.println("Answer is : " + c);

        }
        catch(ArithmeticException e) {
            out.println("Can't divide by 0.");
        }

    }

}
