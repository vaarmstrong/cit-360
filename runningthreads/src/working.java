import java.util.*;
import java.lang.*;

public class working implements Runnable{
    private String color;
    private int number;
    private int sleep;
    private int rando;



    public working(String color, int number, int sleep) {

        this.color = color;
        this.number = number;
        this.sleep = sleep;

        Random random = new Random();
        this.rando = random.nextInt(1000);
    }

    public void run() {
        System.out.println("\n\nColors: " + color + " Number = " + number + " Sleep = " + sleep + " Random Number = " + rando + "\n\n");
        for (int count = 1; count < rando; count++) {
            if (count % number == 0) {
                System.out.println(color + " is sleeping. ");
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n\n" + color + " is done.\n\n");
    }
}



