import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class isworking implements Runnable {
    public static void main(String [] args) {
        ExecutorService myService = Executors.newFixedThreadPool(3);

        working w1 = new working("Blue", 1, 1000);
        working w2 = new working("Yellow", 2, 700);
        working w3 = new working("Green", 4, 500);
        working w4 = new working("Pink", 8, 300);
        working w5 = new working("Purple", 12, 100);
        working w6 = new working("Red", 16, 50);

        myService.execute(w1);
        myService.execute(w2);
        myService.execute(w3);
        myService.execute(w4);
        myService.execute(w5);
        myService.execute(w6);

        myService.shutdown();
    }

    @Override
    public void run() {

    }
}
