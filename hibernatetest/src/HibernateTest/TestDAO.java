package HibernateTest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class TestDAO {

    SessionFactory factory = null;
    Session session = null;

    private static TestDAO single_instance = null;

    private TestDAO() {
        factory = HibernateUtils.getSessionFactory();
    }

    public static TestDAO getInstance() {
        if (single_instance == null) {
            single_instance = new TestDAO();
        }

        return single_instance;
    }

    public  List<Film> getFilms() {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from HibernateTest.Film";
            List<Film> fm = (List<Film>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return fm;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Film getFilm(int id){

            try {
                session = factory.openSession();
                session.getTransaction().begin();
                String sql = "from HibernateTest.Film where id=" + Integer.toString(id);
                Film f = (Film)session.createQuery(sql).getSingleResult();
                session.getTransaction().commit();
                return (Film) f;

            } catch (Exception e) {
                e.printStackTrace();
                session.getTransaction().rollback();
                return null;
            } finally {
                session.close();
            }
        }

}

