package HibernateTest;

import java.sql.*;

public class RunHibernate {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String dburl = "jdbc:mysql://localhost/";
    static final String dbuser = "root";
    static final String dbpass = "password";

    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;

        try {
            System.out.println("Connecting database...");
            Connection conn = DriverManager.getConnection(dburl, dbuser, dbpass);
            System.out.println("Database connected!");
            statement = conn.createStatement();

            //This is the insert to mysql.
            String query = "INSERT INTO FILM(title, description, release_year) values('Knives Out', 'A detective investigates the death of a apatriarch of an exxentric, combative family.', '2019')";
            String query1 = "INSERT INTO FILM(title, description, release_year) values('Cruela', 'A live-action prequel feature film following a young Cruella de Vil', '202')";
            String query2 = "SELECT title, description, release_year FROM film";
            String updatequery = "UPDATE FILM SET TIT:E = 'Cruella' WHERE ID = 1002";

            statement.executeUpdate(query);
            statement.executeUpdate(query1);
            ResultSet resultSet = statement.executeQuery(query2);
            statement.executeUpdate(updatequery);

            while(resultSet()) {
                System.out.println("ID: " + resultSet.getInt("id"));
                System.out.println("Title: " + resultSet.getString("title"));
                System.out.println("Description: " + resultSet.getString("description"));
                System.out.println("Release Year: " + resultSet.getInt("release_year"));

            }

            System.out.println("Record Inserted and Updated Successfully");
        }

        catch (SQLException e)
        {
            System.err.println("Cannot connect to the database!");
            e.printStackTrace();
        }

        finally {
            System.out.println("Closing the connection.");
            if (connection != null) try { connection.close(); } catch (SQLException ignore) {}
        }
    }

    private static boolean resultSet() {
        return false;
    }
}
