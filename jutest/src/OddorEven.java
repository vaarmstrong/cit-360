import java.util.Scanner;

public class OddorEven {

    public static boolean OddEven (int x) {
        if (x % 2 == 0)
            return true;
        return false;
    }

    public static void main(String[] args) {
        int x = 0;
        boolean result = false;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Is your integer even or odd?");
        System.out.println("Please enter an integer: ");
        x = scanner.nextInt();

        result = OddEven(x);

        if(result)
            System.out.println(x + " is an even integer.");
        else
            System.out.println(x + " is an odd integer.");
    }
}