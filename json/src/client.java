import java.io.StringWriter;
import java.lang.*;

import org.codehaus.jackson.map.ObjectMapper;

public class client {

    public static void main(String[] args) {
        try {
            dinero obj_Dinero = new dinero();

            obj_Dinero.setTime("May 15, 2021");
            obj_Dinero.setCode("GBP");
            obj_Dinero.setSymbol("&pound;");
            obj_Dinero.setRate("35,133.9752");
            obj_Dinero.setDescription("British Pound Sterling");
            obj_Dinero.setRate_Float("35133.9752");

            ObjectMapper objectMapper = new ObjectMapper();
            StringWriter stringDin = new StringWriter();
            objectMapper.writeValue(stringDin, obj_Dinero);
            System.out.println("Currency is: " + stringDin);
        } catch (Exception e) {
            System.out.println(e);
        }

    }


}
