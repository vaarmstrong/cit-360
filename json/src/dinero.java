import java.lang.*;

public class dinero {

    private String time;
    private String code;
    private String symbol;
    private String rate;
    private String description;
    private String rate_Float;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRate_Float() {
        return rate_Float;
    }

    public void setRate_Float(String rate_Float) {
        this.rate_Float = rate_Float;
    }
}
